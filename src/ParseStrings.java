import java.util.Scanner;
import java.util.StringTokenizer;

public class ParseStrings {

    //code by Matthew Pipitone
    public static Scanner in = new Scanner(System.in);

    public static boolean hasCharacter(String str,char character) {
        int i;
        boolean has = false;
        for (i = 0; i < str.length(); i++) {
            //System.out.println(str.charAt(i));
            if (str.charAt(i) == character)
                has = true;
            //System.out.println(has);
        }

        return has;
    }
    public static void main(String[] args) {
        /* Type your code here. */
        String str, firstWord, secondWord;
        boolean done, fWord,stringTokken;
        int i;

        done = false;

        while (!done) {
            firstWord = "";
            secondWord = "";

            //System.out.println("Input phase");
            do {
                System.out.println("Enter input string:");
                str = in.nextLine();

                //System.out.println("Check phase");
                if (str.length() == 1 && hasCharacter(str,'q'))
                    return;

                //System.out.println("Testing value phase");
                boolean comma = hasCharacter(str,',');
                if (!hasCharacter(str,','))
                    System.out.println("Error: No comma in string.\n");
                //System.out.println(comma + " " + str);
            } while (!hasCharacter(str,','));

            String results[] = str.split(",");
            str = results[0] + " " + results[1];

            stringTokken = false;

            while (!stringTokken){
                StringTokenizer st = new StringTokenizer(str);
                fWord = false;
                while (st.hasMoreTokens()) {
                    if (!fWord) {
                        firstWord = st.nextToken();
                        fWord = true;
                    } else {
                        secondWord = st.nextToken();
                        stringTokken = true;
                    }
                }
            }

            System.out.println("First word: " + firstWord);
            System.out.println("Second word: " + secondWord);

            System.out.println("");

        }

    }

}
