import java.util.Scanner;
import java.util.ArrayList;
import java.util.StringTokenizer;
public class DataVisualizer {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {


        /* Type your code here. */

        ArrayList<String> stringArray = new ArrayList<String>();
        ArrayList<Integer> intArray = new ArrayList<Integer>();
        StringTokenizer st;

        String title, head1, head2, data;

        System.out.println("Enter a title for the data:");
        title = in.nextLine();

        System.out.println("You entered: " + title + "\n");

        System.out.println("Enter the column 1 header:");
        head1 = in.nextLine();

        System.out.println("You entered: " + head1 + "\n");

        System.out.println("Enter the column 2 header:");
        head2 = in.nextLine();

        System.out.println("You entered: " + head2 + "\n");

        boolean done = false;
        while (!done) {
            String splitter[];
            String str, tempStr, tempVal;
            int num = 0;
            boolean done2 = false;
            boolean swtch = false;
            boolean error = false;
            tempStr = "";
            tempVal = "";
            do {

                System.out.println("Enter a data point (-1 to stop input):");
                data = in.nextLine();

                if (data.contains("-1")) {
                    System.out.println();
                    done2 = true;
                    break;
                }

                if (findNumOfCharacters(data, ',') == 0) {
                    System.out.println("Error: No comma in string.\n");
                    error = true;
                    break;
                } else if (findNumOfCharacters(data, ',') >= 2) {
                    System.out.println("Error: Too many commas in input.\n");
                    error = true;
                    break;
                }

                splitter = data.split(",");


                //System.out.println("Phase 4:");
                for (int i = 0; i < splitter.length; i++) {
                    if (swtch == false) {
                        tempStr = splitter[i];
                        swtch = true;
                    } else {
                        tempVal = splitter[i];
                    }
                }

                if (isInt(tempVal)) {
                    num = Integer.parseInt(tempVal);

                } else {
                    error = true;
                }
            } while (findNumOfCharacters(data, ',') != 1);
            if (done2) {
                break;
            }


            swtch = false;

            if (error != true) {
                stringArray.add(tempStr);
                intArray.add(num);
                System.out.println("Data string: " + tempStr);
                System.out.println("Data integer: " + num + "\n");
            } else {
                error = false;
            }

        }
        int spaces;

        boolean printing = true;
        System.out.println("        " + title);
        System.out.println(head1 + "         |       " + head2);
        System.out.println("--------------------------------------------");

        while (printing) {
            for (int i = 0; i < stringArray.size(); i++) {
                System.out.print(stringArray.get(i));
                spaces = 20 - stringArray.get(i).length() - 1;
                for (int s = 0; s <= spaces; s++) {
                    System.out.print(" ");
                }

                System.out.print("|");

                spaces = 23 - intArray.get(i).toString().length();

                for (int s = 0; s < spaces; s++) {
                    System.out.print(" ");

                    if (s == spaces - 1 && (i != stringArray.size() - 1)) {
                        System.out.print(intArray.get(i) + "\n");

                    } else if ((i == stringArray.size() - 1) && (s == spaces - 1)) {
                        System.out.print(intArray.get(i) +"\n");
                        printing = false;
                    }
                }
            }

            printing = true;
            while (printing) {
                spaces = 20 - stringArray.size();
                for (int i = 0; i < spaces; i++) {
                    System.out.print(" ");
                }

                for (int i = 0; i < intArray.size(); i++) {
                    System.out.print(stringArray.get(i) + " ");
                    for (int j = 0; j < intArray.get(i);j++) {
                        System.out.print("*");

                        if (j == intArray.get(i) + 1) {
                            System.out.print("\n");
                        }
                    }
                    if (i == intArray.size()) {
                        printing = false;
                    }

                }


                printing = false;
            }



            return;
        }
    }

    public static boolean isInt(String str){
        try {
            Integer.parseInt(str);

            return true;
        } catch (NumberFormatException e) {
            System.out.println("Error: Comma not followed by an integer.\n");
            return  false;
        }
    }

    public static int findNumOfCharacters(String str, char character){
        int i, count;

        count = 0;
        for (i = 0; i < str.length(); i++) {
            if (str.charAt(i) == character) {
                count += 1;
            }
        }

        return count;
    }
}